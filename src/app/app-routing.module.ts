import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { SignupComponent } from './signup/signup.component';
import { CommonModule } from '@angular/common';


const routes: Routes = [
  {path:'',component: SignupComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes),CommonModule,FormsModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
