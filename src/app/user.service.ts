import { Injectable } from '@angular/core';
import { User } from './user';
import { HttpClient, HttpClientModule } from '@angular/common/http';
// import { User } from 'user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }
  username1:String='';

  //signup
  saveuser(user1: User) {
    return this.http.post<boolean>('http://localhost:8080/signup',user1);
  }

}
